import config from './config';

export default class LH {
  constructor(pooledFeatures) {
    this.features = [];

    if (pooledFeatures) {
      this.pushAll(pooledFeatures);
    }
  }

  push(id) {
    const feature = config.features.find(configFeature => configFeature.id === id);

    if (feature && !this.isEnabled(feature.id)) {
      this.features.push(feature);
    }
  }

  pushAll(ids) {
    ids.map(id => this.push(id));
  }

  isEnabled(id) {
    return !!this.features.find(feature => feature.id === id);
  }

  getEnabledIDs() {
    return this.features
      .map(feature => feature.internalId)
      .filter(internalId => !!internalId)
      .sort()
      .reduce((result, internalId) => {
        return result + internalId;
      }, '');
  }
}
