export default {
  features: [
    {
      id: 'largeButtons',
      internalId: null,
    },
    {
      id: 'sortByPrice',
      internalId: 'A',
    },
    {
      id: 'showRecentlyViewed',
      internalId: 'F',
    },
    {
      id: 'shortMasthead',
      internalId: 'D',
    },
  ],
};
