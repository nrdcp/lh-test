/* eslint prefer-arrow-callback: 0 */

import LH from './lh';
import config from './config';

const mockFeatures = [
  {
    id: 'F1',
    internalId: null,
  },
  {
    id: 'F2',
    internalId: 'Z',
  },
  {
    id: 'F3',
    internalId: 'A',
  },
  {
    id: 'F4',
    internalId: 'B',
  },
];

let lh;

describe('LH', function () {
  beforeEach(function () {
    config.features = mockFeatures;
  });

  describe('getEnabledIDs', function () {
    beforeEach(function () {
      lh = new LH();
    });

    it('returns a string', function () {
      expect(lh.getEnabledIDs()).to.be.a('string');
    });

    it('returns only features which contain external id', function () {
      lh.push('F2');
      lh.push('F3');
      lh.push('F4');
      expect(lh.getEnabledIDs()).to.equal('ABZ');
    });

    it('sorts internal ids alphabetically', function () {
      lh.push('F1');
      lh.push('F2');
      lh.push('F3');
      expect(lh.getEnabledIDs()).to.equal('AZ');
    });
  });

  describe('push', function () {
    beforeEach(function () {
      lh = new LH();
    });

    it('adds feature to the list of enabled features', function () {
      lh.push('F4');
      expect(lh.features.length).to.equal(1);
    });

    it('adds only known features', function () {
      lh.push('F_INVALID');
      expect(lh.features.length).to.equal(0);
    });

    it('adds feature only once', function () {
      lh.push('F4');
      lh.push('F4');
      lh.push('F4');
      expect(lh.features.length).to.equal(1);
    });
  });

  describe('pushAll', function () {
    beforeEach(function () {
      lh = new LH();
    });

    it('adds array of valid features to the list of enabled features', function () {
      lh.pushAll(['F2', 'F3', 'F4']);
      expect(lh.features.length).to.equal(3);
    });
  });

  describe('isEnabled', function () {
    beforeEach(function () {
      lh = new LH();
    });

    it('returns true if feature is enabled', function () {
      lh.push('F2');
      expect(lh.isEnabled('F2')).to.be.true;
    });

    it('returns false if feature is not enabled', function () {
      expect(lh.isEnabled('F2')).to.be.false;
    });

    it('returns false if feature is not valid', function () {
      expect(lh.isEnabled('F_INVALID')).to.be.false;
    });
  });

  describe('constructor', function () {
    beforeEach(function () {
      const pooledFeatures = ['F1', 'F2', 'F3'];
      lh = new LH(pooledFeatures);
    });

    it('adds pooled features upon initialization', function () {
      expect(lh.features.length).to.equal(3);
    });
  });
});
