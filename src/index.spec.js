/* eslint prefer-arrow-callback: 0 */

import index from './index';
import LH from './lh';

let lhSpy;

describe('index', function () {
  beforeEach(function () {
    sinon.createStubInstance(LH);
    lhSpy = sinon.spy(LH);
  });

  it('creates a new instance of LH', function () {
    expect(global.window.LH).to.be.an.instanceof(LH);
  });

  it('creates a new instance of LH via new', function () {
    expect(lhSpy).to.have.been.calledWithNew;
  });

  // TOOD: get sinon-chai to check call arguments
  xdescribe('window.LH already present and is an array', function () {
    beforeEach(function () {
      window.LH = ['test'];
    });

    it('instance created with value of existing LH', function () {
      expect(lhSpy).to.have.been.calledWith(['test']);
    });
  });
});
