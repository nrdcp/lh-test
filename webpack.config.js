const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {

  context: path.resolve(__dirname, 'src'),

  entry: {
    bundle: [
      // 'babel-polyfill',
      './index.js',
    ],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /(\/node_modules\/|test\.js|\.spec\.js$)/,
      },
    ],
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['.js'],
    modules: [
      __dirname,
      path.resolve(__dirname, './node_modules'),
    ],
  },

  devServer: {
    contentBase: path.resolve(__dirname, './src'),
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
  },

  devtool: 'eval-source-map',

  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      hash: true,
      filename: 'index.html',
      template: path.resolve(__dirname, 'index.html'),
    }),
  ],
};

if (process.env.NODE_ENV === 'production') {
  config.devtool = '';
}

module.exports = config;
