## LH script

### Installation

Requires node.js v6 or newer
Run `npm install` to install project dependecies

### Running Demo
Run `npm run start` and navigate to `http://localhost:3000`.
The demo imitates scenario when LH script is loaded asynchronously.

### Running unit tests
Run `npm run test` for single run
Run `npm run test:watch` for continuous watch

### Building the project
Run `npm run build`, then check contents of `/dist/` folder.

### Time spent on task
1. Initial in-browser POC script (ES5): 0.25h
2. Setting up ES6 project and unit testing facilities: 3h (I did not have a relevant boilerplate at hand)
3. Writing full implementation and unit tests: 2h
4. Final checks, git: 1h
